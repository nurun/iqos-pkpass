#!groovy
node {

    stage('Checkout'){

        checkout scm
    }
    
    stage('Prepare') {
        if (!fileExists ('deploy.php')) {
            error ('Deployer file doesn\'t exist. Please ensure you have created a deploy file via deployer')
        }
        if (!fileExists ('sonar-project.properties')) {
            error ('Sonar file doesn\'t exist. Please ensure you have created a sonar properties file')
        }
        sh (
            script: "dep jenkins:init ${env.BRANCH_NAME}",
            returnStatus: true
        )
    }

    stage('Sonar: Analysis'){
        def sonarScanned = sh (
                script: "/usr/bin/sonar-scanner -Dsonar.branch=${env.BRANCH_NAME} -Dsonar.projectVersion=${env.BUILD_NUMBER}",
                returnStatus: true
            ) ==0
        if(sonarScanned) {
            echo 'SONAR ANALYSIS SUCCESSFUL'
        } else 
        {
            error ('SONAR ANALYSIS FAILED')
        }
    }

    stage('Sonar: Check Quality Gates') {

        def props = getBuildProperties("sonar-project.properties")
            String projectKey = props.getProperty('sonar.projectKey')
            String username = props.getProperty('sonar.login')
            String password = props.getProperty('sonar.password')
            String[] strictQualityGate = props.getProperty('sonar.strictQualityGate').split(",");

        //do quality gate checking
        try {
            def output = sh (
                        script: "curl -k -X GET -u ${username}:${password} 'https://sonar.nurun.io/api/qualitygates/project_status?projectKey=${projectKey}'",
                        returnStdout: true
            )
            if(output != 0){
                def qualityGate = new groovy.json.JsonSlurper()
                def parsedResponse = qualityGate.parseText(output)
                if(parsedResponse.projectStatus.status != 'OK'){
                    for(condition in parsedResponse.projectStatus.conditions) {
                        if(condition.status == 'WARN') {
                            echo "QUALITY GATE NOTICE: ${condition.metricKey} (${condition.actualValue}) is ${condition.comparator} ${condition.warningThreshold}"
                        }
                        if(condition.status == 'ERROR') {
                            String errorMessage = "${condition.metricKey} (${condition.actualValue}) is ${condition.comparator} ${condition.errorThreshold}"
                            if(!strictQualityGate.contains(env.BRANCH_NAME)) {
                                    echo "QUALITY GATE FAILED: ${errorMessage}"
                                    echo "QUALITY GATE NOTICE: PRODUCTION DEPLOYMENT WILL FAIL"
                            } else {
                                error("QUALITY GATE FAILED: ${errorMessage}")
                            }
                        }
                    }
                } else {
                    echo "QUALITY GATE SUCCESSFUL"
                }
            }
        } catch (Exception e) {
                echo "FAILURE: ${e}"
                throw e; // rethrow so the build is considered failed                        
        } 
        
    }
    
    stage('Deploy (if applicable)') {
        sh (
            script: "dep deploy ${env.BRANCH_NAME}",
            returnStatus: true
        )
    }
}    

def Properties getBuildProperties(filename) {
    def properties = new Properties()
    properties.load(new StringReader(readFile(filename)))
    return properties
}