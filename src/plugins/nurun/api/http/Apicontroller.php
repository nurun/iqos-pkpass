<?php namespace Nurun\Api\Http;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use nurun\pkpass\classes\PKPassGenerator;
use Redirect, Validator, Mail, File;
use Cms\Classes\Theme;

/**
 * Apicontroller Back-end Controller
 */
class Apicontroller extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';

    /**
     * used to catch all routes that are not allowed and throws 404 response
     */
    public function noDragonsAllowed()
    {

        return response()->json(['Success' => false, 'ErrorMessage' => 'Hey, what are you doing here? Naughty naughty!'], 404);

    }

    public function post(Request $request)
    {
        $data = $request->json()->all();

        Validator::extend('imageBlob', function ($attribute, $value) {

            $valid = false;
            try {
                $file = base64_decode($value);
                $image = imagecreatefromstring($file);
                $valid = ($image !== false);

            }catch(\Exception $e) {

                $valid = false;

            } finally {

                return $valid;

            }

        },'Device image field is not a valid image file');


        $validator = Validator::make($data, [
            'EmailAddress' => ['required', 'email'],
            'FirstName' => ['required'],
            'LastName' => ['required'],
            'DeviceName' => ['required'],
            'DeviceImage' => ['required','imageBlob'],
            'PurchaseDate' => ['required','date'],
            'WarrantyExpiryDate' => ['required','date'],
        ]);

        $errors = '';
        if ($validator->fails()) {
            $errors = implode('. ', $validator->messages()->all());
        }

        if (!empty($errors))
            return response()->json(['Success' => false, 'ErrorMessage' => $errors], 400);

        //split data sets
        $user = (object) [
            'EmailAddress' => $data['EmailAddress'],
            'FirstName' => $data['FirstName'],
            'LastName' => $data['LastName'],
        ];

        $device = (object) [
            'DeviceName' => $data['DeviceName'],
            'DeviceImage' => $data['DeviceImage'],
            'PurchaseDate' => Carbon::parse($data['PurchaseDate'])->format('Y/m/d'),
            'WarrantyExpiryDate' => Carbon::parse($data['WarrantyExpiryDate'])->format('Y/m/d'),
        ];


        //generate file
        $pkpass = new PKPassGenerator($user, $device);
        $file =  $pkpass->generate();

        $path = url('/themes/'. Theme::getActiveTheme()->getDirName());

        //send email
        Mail::send('nurun.pkpass::mail.message', ['user' => $user, 'device' => $device, 'path' => $path], function ($message) use ($user, $file) {

            $message->to($user->EmailAddress, $user->FirstName.' '.$user->LastName)
                ->subject(
                    'iQOS Device information digital pass'
                )
                ->attach($file);
        });

        File::delete($file);


        /** Return JSON response */
        return response()->json(['Success' => true]);

    }
}
