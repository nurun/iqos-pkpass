<?php namespace Nurun\Api\Http\middleware;

use Closure;
use Nurun\Api\Models\AppUser;
use Redirect,Validator;

class AppAuth
{
    public function handle($request, Closure $next)
    {
        if (!$request->json()) {
            return redirect('/');
        }

        $data = (object)$request->json()->all();
        $userToken = $request->bearerToken();
        $errors = [];

        //check for token
        if (!$userToken)
            return response("User access denied\n", 403);

        // find user
        $AppUser = AppUser::where('token', $userToken)->first();

        // cant find user, reject
        if (!$AppUser) {

            $errors[] = "Vendor not Authorized\n";

        }
        // if errors return the error
        if (count($errors) > 0)
            return response()->json(['Success' => false, 'ErrorMessage' => $errors], 400);

        return $next($request);
    }
}