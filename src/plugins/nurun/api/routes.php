<?php

Route::group(['middleware'=> 'Nurun\Api\Http\middleware\AppAuth', 'prefix' => 'api'], function () {

    Route::post('send_digital_card', 'Nurun\Api\Http\Apicontroller@post');

    /**
     * catch all for non approved/defined endpoints
     */
    Route::any('{all}', [

        'uses' => 'Nurun\Api\Http\Apicontroller@noDragonsAllowed',

    ])->where('all', '(.*)');

});