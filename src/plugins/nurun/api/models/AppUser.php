<?php namespace Nurun\Api\Models;

use Model;

/**
 * Model
 */
class AppUser extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    protected $jsonable =  ['userdata'];
    protected $guarded = ['password','permissions','created_at','deleted_at','updated_at'];
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nurun_api_users';
}