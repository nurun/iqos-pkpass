<?php namespace Nurun\Api\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class AppUsers extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    
    public function __construct()
    {
        parent::__construct();
        $this->vars['var'] = 'value';
    }
    public function onTokenGenerate(){
    	$token = str_random(60);
    	return $token;
    }
}