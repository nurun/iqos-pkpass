<?php namespace Nurun\Api\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNurunApiUsers extends Migration
{
    public function up()
    {
        Schema::create('nurun_api_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('app_name', 255); 
            $table->string('token', 255)->nullable();    
            $table->timestamps();      
            $table->softDeletes();     
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nurun_api_users');
    }
}