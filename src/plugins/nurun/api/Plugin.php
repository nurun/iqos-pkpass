<?php namespace Nurun\Api;

use System\Classes\PluginBase;
use Backend;
class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name' => 'Nurun API Plugin',
        	'description' => 'Webservice for Pass File',
            'author'      => 'Johan Steyn',
            'icon'        => 'icon-fire',
            'homepage'    => ''
        ];
    }
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }
    public function registerNavigation()
	{
	    return [
	        'API' => [ 
	            'label'       => 'API Users',
	            'url'         => Backend::url('nurun/api/appusers'),
	            'icon'        => 'icon-cloud',
	            'order'       => 800,
	            ]
	        ];
	}
	public function boot(){
		//$kernel = $this->app->make('Illuminate\Contracts\Http\Kernel');
		//$kernel->pushMiddleware(http\middleware\AppAuth::class);
		$this->app['router']->middleware('Api.Auth', '\Nurun\Api\Http\middleware\AppAuth');
	}
}
