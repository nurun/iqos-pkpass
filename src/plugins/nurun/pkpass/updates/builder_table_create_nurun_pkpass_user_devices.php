<?php namespace Nurun\Pkpass\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNurunPkpassUserDevices extends Migration
{
    public function up()
    {
        Schema::create('nurun_pkpass_user_devices', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('device_model_id');
            $table->string('device_id');
            $table->date('purchase_date');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nurun_pkpass_user_devices');
    }
}
