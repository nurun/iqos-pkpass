<?php namespace Nurun\Pkpass\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNurunPkpassUserDevices extends Migration
{
    public function up()
    {
        Schema::table('nurun_pkpass_user_devices', function($table)
        {
            $table->date('warranty_date');
        });
    }
    
    public function down()
    {
        Schema::table('nurun_pkpass_user_devices', function($table)
        {
            $table->dropColumn('warranty_date');
        });
    }
}
