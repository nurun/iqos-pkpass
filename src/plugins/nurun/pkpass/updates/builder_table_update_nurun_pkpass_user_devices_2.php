<?php namespace Nurun\Pkpass\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNurunPkpassUserDevices2 extends Migration
{
    public function up()
    {
        Schema::table('nurun_pkpass_user_devices', function($table)
        {
            $table->string('device_id', 255)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('nurun_pkpass_user_devices', function($table)
        {
            $table->string('device_id', 255)->nullable(false)->change();
        });
    }
}
