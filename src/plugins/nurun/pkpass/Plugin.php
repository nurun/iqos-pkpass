<?php namespace Nurun\Pkpass;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Nurun\Pkpass\Components\RegisterDevice' => 'RegisterDeviceForm'
        ];
    }

    public function registerMailTemplates()
    {
        return [
            'nurun.pkpass::mail.message' => 'Pkpass template.'
        ];
    }

    public function registerSettings()
    {

    }

    public function boot() {


    }
}
