<?php
/**
 * Created by PhpStorm.
 * User: johsteyn
 * Date: 2018/09/27
 * Time: 08:31
 */

namespace nurun\pkpass\classes;

use Nurun\Pkpass\Models\UserDevices;
use October\Rain\Database\Attach\File;
use PKPass\PKPass;
use Rainlab\User\Models\User;
use System\Classes\MediaLibrary;


class PKPassGenerator
{
    private $user, $device;

    /**
     * PKPassGenerator constructor.
     * @param stdClass $user
     * @param stdClass $device
     */
    public function __construct($user, $device)
    {
        $this->user = $user;
        $this->device = $device;
    }


    /**
     * @param bool $output
     * @return bool|string
     */
    public function generate($output = false) {

        //Set certificate and path in the constructor
        $pass = new PKPass(__DIR__ .'/../certs/Certificates.p12', 'iQ0S2018');
        $serialNumber = uniqid('IQOS');

        $data = [
            'description' => 'iQOS Care Plus',
            'formatVersion' => 1,
            'organizationName' => 'Philip Morris',
            'passTypeIdentifier' => 'pass.com.iqos.za',
            'serialNumber' => $serialNumber,
            'teamIdentifier' => 'GSHR98KYU5', // Change this!
            'generic' => [
                'headerFields' => [

                ],
                'primaryFields' => [
                    [
                        'key' => 'customer_details',
                        'label' => $this->user->FirstName,
                        'value' => $this->user->LastName,
                    ]
                ],
                'secondaryFields'  => [
                    [
                        'key' => 'purchase_date',
                        'label' => 'Purchase date',
                        'value' => $this->device->PurchaseDate,
                        'dataDetectorTypes' => ['PKDataDetectorTypeCalendarEvent'],
                        'textAlignment' => 'PKTextAlignmentLeft'
                    ],
                    [
                        'key' => 'warranty_date',
                        'label' => 'Warranty date',
                        'value' => $this->device->WarrantyExpiryDate,
                        'dataDetectorTypes' => ['PKDataDetectorTypeCalendarEvent'],
                        'textAlignment' => 'PKTextAlignmentRight'
                    ],
                ],
                'auxiliaryFields' => [
                    [
                        'key' => 'callcentre_copy',
                        'label' => 'Instructions',
                        'value' => "Flip the card for support details",
                    ],
                ],
                'backFields' => [
                    [
                        'key' => 'phone',
                        'label' => 'Call us',
                        'value' => '0800 043 387',
                        'dataDetectorTypes' => ['PKDataDetectorTypePhoneNumber']
                    ],
                    [
                        'key' => 'international',
                        'label' => 'International Assistance ',
                        'value' => '+800 2559 2559',
                        'dataDetectorTypes' => ['PKDataDetectorTypePhoneNumber']


                    ],
                    [
                        'key' => 'web',
                        'label' => 'iQOS Support',
                        'value' => 'https://za.iqos.com/support-iqos?GTM=iOSWallet',
                        'dataDetectorTypes' => ['PKDataDetectorTypeLink']
                    ],
                    [
                        'key' => 'facebook',
                        'label' => 'Facebook',
                        'value' => 'https://www.facebook.com/iqos.za',
                        'dataDetectorTypes' => ['PKDataDetectorTypeLink']
                    ],
                    [
                        'key' => 'twitter',
                        'label' => 'Twitter',
                        'value' => 'https://twitter.com/iqos_support_za',
                        'dataDetectorTypes' => ['PKDataDetectorTypeLink']
                    ],
                ],
            ],
            'backgroundColor' => 'rgb(245,245,243)',
            'logoText' => 'iQOS Care Plus',
            'relevantDate' => date('Y-m-d\TH:i:sP')
        ];

        $pass->setTempPath(temp_path());
        $pass->setData($data);


        $thumb = new File();

        $thumbOptions = [
            'mode' => 'fit',
            'extension' => 'png'
        ];

        $file = $thumb->fromData(base64_decode($this->device->DeviceImage), $serialNumber.'.png');
        $fileName = $thumb->getThumb(270,270,$thumbOptions);
        $fileName = str_replace($thumb->getPublicPath(), '',$fileName);
        $thumb270 = storage_path('app/uploads/public/' . $fileName);

        $thumb->fromData(base64_decode($this->device->DeviceImage), $serialNumber.'.png');
        $fileName = $thumb->getThumb(180,180,$thumbOptions);
        $fileName = str_replace($thumb->getPublicPath(), '',$fileName);
        $thumb180 = storage_path('app/uploads/public/' . $fileName);

        $thumb->fromData(base64_decode($this->device->DeviceImage), $serialNumber.'.png');
        $fileName = $thumb->getThumb(90,90,$thumbOptions);
        $fileName = str_replace($thumb->getPublicPath(), '',$fileName);
        $thumb90 = storage_path('app/uploads/public/' . $fileName);

        // add files to the PKPass package
        $pass->addFile(__DIR__ .'/../images/icon.png');
        $pass->addFile(__DIR__ .'/../images/icon@2x.png');
        $pass->addFile(__DIR__ .'/../images/logo.png');
        $pass->addFile(__DIR__ .'/../images/background.png');
        $pass->addFile($thumb90,'thumbnail.png');
        $pass->addFile($thumb180,'thumbnail@2x.png');
        $pass->addFile($thumb270,'thumbnail@3x.png');


        $result = $pass->create();
        $fileName = temp_path().'/'. $serialNumber .'.pkpass';

        file_put_contents($fileName,$result);

        if($output) {
            return $result;
        } else {
            return $fileName;
        }
    }
}