<?php
/**
 * Created by PhpStorm.
 * User: johsteyn
 * Date: 2018/10/02
 * Time: 08:58
 */

namespace Nurun\Pkpass\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Theme;
use Request;
use Input;
use Mail;
use nurun\pkpass\classes\PKPassGenerator;
use Nurun\Pkpass\Models\DeviceModels;
use Nurun\Pkpass\Models\UserDevices;
use Validator;
use Flash;
use Redirect;
use Rainlab\User\Models\User;
use Auth;
use File;


class RegisterDevice extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Create a Pkpass',
            'description' => 'Simple form to create a user and sent them a pass file'
        ];
    }

    public function onGetModelOptions()
    {
        //first validate request
        $input = Input::all();

        if(isset($input['name']) && $input['name'] !='') {

            $models = DeviceModels::where('name', 'LIKE', '%'.$input['name'].'%')->select(['id','name']);
        } else {

            $models = DeviceModels::select(['id','name']);
        }

        $data = $models->get();
        return $data;

    }

    public function onRegisterDevice() {

        //first validate request
        $input = Input::all();

        $validator = Validator::make($input,
            [
                'name' => 'required',
                'email' => 'required|email',
            ]
        );

        if ($validator->fails()) {

            Flash::error('Missing fields!');

        } else {

            //create/update user record
            $user = Auth::findUserByLogin($input['email']);

            if ($user) {

                $user->fill($input);
                $user->save();

            } else {

                //generate a unique password based on data input and timestamp
                $password = sha1(json_encode($input).time());

                $user = Auth::register(array_merge([
                    'password' => $password,
                    'password_confirmation' => $password
                ],$input));

            }

            $this->registerDevice($user, $input);
            Flash::success('Device captured.');
        }

    }

    private function registerDevice(User $user, array $deviceInfo) {

        $device = UserDevices::updateOrCreate([
            'user_id' => $user->id,
            'device_model_id' => $deviceInfo['device_model_id'],
            'purchase_date' => $deviceInfo['purchase_date'],
            'device_id' => ''
        ],$deviceInfo);


        //generate file
        $pkpass = new PKPassGenerator($user, $device);
        $file =  $pkpass->generate();

        $path = url('/themes/'. Theme::getActiveTheme()->getDirName());

        //send email
        Mail::send('nurun.pkpass::mail.message', ['user' => $user, 'device' => $device, 'path' => $path], function ($message) use ($user, $file) {

            $message->to($user->email, $user->name.' '.$user->surname)
                ->subject(
                    'iQOS Device information digital pass'
                )
                ->attach($file);
        });

        File::delete($file);

    }
}