<?php namespace Nurun\Pkpass\Models;

use Model;

/**
 * Model
 */
class DeviceModels extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'product_image' => ['required'],
    ];

    public $customMessages = [
        'name.required' => 'A name of the device is required.',
        'product_image.required' => 'A product image is required.',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nurun_pkpass_device_models';

    protected $fillable = ['name','product_image'];

    public $hasMany = [
        'devices' => [
            'Nurun\Pkpass\Models\UserDevices'
        ],
    ];
}
