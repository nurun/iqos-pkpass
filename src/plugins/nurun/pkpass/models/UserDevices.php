<?php namespace Nurun\Pkpass\Models;

use Model;

/**
 * Model
 */
class UserDevices extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'user' => 'required',
        'device_model' => ['required'],
//        'device_id' => ['required'],
        'purchase_date' => ['required','date'],
        'warranty_date' => ['required','date'],
    ];

    public $customMessages = [
        'user.required' => 'A user is required.',
        'device_model.required' => 'A device model is required.',
 //       'device_id.required' => 'The devices ID is required.',
        'purchase_date' => 'A purchase date is required.',
        'warranty_date' => 'A warranty date is required.',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nurun_pkpass_user_devices';


    protected $fillable = ['user_id','device_model_id','device_id','purchase_date','warranty_date'];

    public $belongsTo = [
        'user' => [
            'Rainlab\User\Models\User'
        ],
        'device_model' => [
            'Nurun\Pkpass\Models\DeviceModels'
        ],
    ];
}
