$(function() {

    $('#send_pass').validate();

    var warrentyPicker = $('#warranty_date').pickadate({
        formatSubmit: 'yyyy/mm/dd',
        hiddenName: true,
        selectYears: true,
    }).pickadate('picker');

    $('#purchase_date').pickadate({
        formatSubmit: 'yyyy/mm/dd',
        hiddenName: true,
        selectYears: true,
        onSet: function(context) {

            var day = moment(context.select);
            day.add(12, 'months');

            warrentyPicker.set({
                'min': day.toDate(),
                'select': day.toDate()
            });

        }
    });



    $('#device_model_id').selectize({
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        preload: true,
        create: false,
        options: [],
        load: function(query, callback) {

            $.ajax({
                url: '/',
                type: 'POST',
                dataType: 'json',
                headers: {
                    'X-OCTOBER-REQUEST-HANDLER': 'onGetModelOptions',
                },
                data: {
                    name: query,
                    page_limit: 10,
                },
                error: function() {
                    callback();
                },
                success: function(data) {
                    callback(data);
                }
            });
        }
    });

});