module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-image-embed');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-browserify');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),


        concat: {
            dist: {
                src: [
                    'assets/css/scss/app.scss',
                    'node_modules/bootstrap/scss/bootstrap.scss',
                ],
                dest: 'assets/css/scss/build.scss'
            }
        },

        sass: {
            options: {
                loadPath: 'node_modules/bootstrap/scss'
            },

            dist: {
                options: {
                    style: 'compressed',
                },
                files: {
                    'assets/css/style.css': ['assets/css/scss/build.scss']
                }
            },
            dev: {
                options: {
                    style: 'expanded',
                },
                files: {
                    'assets/css/style.css': ['assets/css/scss/build.scss']
                }
            },
        },

        cssmin: {
            options: {
                sourceMap: false
            },
            target: {
                files: {
                    'assets/css/style.min.css': [
                        'assets/css/addInput.css',
                        'node_modules/selectize.js/dist/css/selectize.css',
                        'node_modules/pickadate/lib/compressed/themes/default.css',
                        'node_modules/pickadate/lib/compressed/themes/default.date.css',
                        'assets/css/style.css'

                    ]
                }
            }
        },

        imageEmbed: {
            dist: {
                src: ["style.css"],
                dest: "style.css",
                options: {
                    deleteAfterEncoding: false
                }
            }
        },

        uglify: {
            // options: {
            //     sourceMap: true,
            //     compress: {
            //         drop_console: true
            //     }
            // },
            main: {
                files: {
                    'assets/javascript/dist/main.min.js': [
                        'assets/javascript/theme/main.js',

                    ]
                }
            },
            lib: {
                files: {
                    'assets/javascript/dist/lib.min.js': [
                        'node_modules/jquery/dist/jquery.js',
                        'node_modules/selectize.js/dist/js/standalone/selectize.js',
                        'node_modules/jquery-validation/dist/jquery.validate.js',
                        'node_modules/jquery-validation/dist/additional-methods.js',
                        'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
                        'node_modules/moment/moment.js',
                        'node_modules/pickadate/lib/compressed/picker.js',
                        'node_modules/pickadate/lib/compressed/picker.date.js',
                        'assets/javascript/theme/addInput.js',
                    ]
                }
            }
        },

        imagemin: {
            dynamic: {
                options: {
                    optimizationLevel: 7
                },
                files: [
                    {
                        expand: true,
                        cwd: 'devimg/',
                        src: ['**/*.{png,jpg}'],
                        dest: 'assets/images/'
                    }
                ]
            }
        },

        watch: {
            sass: {
                files: ['assets/css/scss/**/*.scss'],
                tasks: ['concat', 'sass:dev'],
                options: {
                    spawn: true
                }
            },
            css: {
                files: ['assets/css/*.css'],
                tasks: ['cssmin', 'imageEmbed'],
                options: {
                    spawn: true
                }
            },
            js: {
                files: ['assets/javascript/theme/*', 'assets/javascript/dist/lib.min.js'],
                tasks: ['uglify'],
                // options: {
                //     spawn: true,
                //     compress: {
                //         drop_console: false
                //     }
                // }
            }
        }
    });

    grunt.registerTask('default', ['concat', 'sass:dist', 'cssmin','browserify', 'uglify', 'watch']);
    grunt.registerTask('dist', ['concat', 'sass:dist', 'cssmin', 'imageEmbed', 'uglify', 'imagemin']);
    grunt.registerTask('dev', ['concat', 'sass:dist', 'cssmin', 'imageEmbed', 'uglify', 'imagemin']);
    grunt.registerTask('css', ['concat', 'sass:dist', 'cssmin']);
    grunt.registerTask('img', ['imagemin']);
    grunt.registerTask('script', ['uglify']);
    grunt.registerTask('wait', ['watch']);
};