### Quick start installation

For advanced users, run this in your terminal to install October from command line:

```shell
php -r "eval('?>'.file_get_contents('https://octobercms.com/api/installer'));"
```

If you plan on using a database, run this command:

```shell
php artisan october:install
```

Install Yarn, you will need latest NPM and Node 8+.

```shell
npm -g install yarn
```

Update Theme

```shell
cd src/themes/pmi
yarn install
grunt dist
```

Before running grunt, install Ruby sass


https://github.com/gruntjs/grunt-contrib-sass

```shell
gem install sass
```


Run grunt in theme.

```shell
grunt dist
```